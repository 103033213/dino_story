class enemy {
    constructor() {
        this.enemyData = game.cache.getJSON('enemyData');
        // These variables will be filled by EnemyFactory
        this.atk = 0;
        this.HP = 0;
        this.EXPDrop = 0;
        this.enemies = game.add.physicsGroup();
        this.type = "";

    }

    spawn() {
        const enemy = this.enemies.getFirstExists(false);
        if (enemy) {
            enemy.reset(GAME_WIDTH, GAME_HEIGHT - 80);
            enemy.animations.play('normal');
            enemy.body.velocity.x = -playState.groundManager.groundSpeed;
            enemy.HP = this.HP;
            enemy.isDead = false;
        }
    }

    setupEnemy(dataName, quantity, spriteName) {
        const enemyData = this.enemyData[dataName];

        this.atk = enemyData.atk;
        this.HP = enemyData.HP;
        this.EXPDrop = enemyData.EXPDrop;

        this.enemies.enableBody = true;
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemies.createMultiple(quantity, spriteName);
        this.enemies.setAll('anchor.x', 0);
        this.enemies.setAll('anchor.y', 1);
        this.enemies.setAll('body.maxVelocity.x', playState.groundManager.groundSpeed);
        this.enemies.setAll('outOfBoundsKill', true);
        this.enemies.setAll('checkWorldBounds', true);
        this.enemies.forEach(function (enemy) {
            enemy.weaponSuffered = new Set();
            enemy.isDead = false;
            enemy.enemyClass = this;
        }, this);
    }

    hurt(instance, damage) {
        instance.HP -= damage;
        // TODO or Not todo: display damage text
        //  Response: I actually used two separate display damage/heal texts in player.js, maybe we should write a
        //  specialized code for that to simplify things?
    }

    update() {
        // Check collisions with playState.player
        game.physics.arcade.overlap(playState.player.sprite, this.enemies, function (player, enemy) {
            if(enemy.isDead) return;
            playState.player.hurt(this.atk);
        }, null, this);

        // Check collisions with player's weapon
        playState.player.playerWeaponManager.liveInstance.forEach((weapon) => {
            game.physics.arcade.overlap(weapon, this.enemies, function (weapon, enemy) {
                if(enemy.isDead) return;

                // check if enemy is hurt by the same weapon
                if (enemy.weaponSuffered.has(weapon)) return;

                switch(weapon._data.name){
                    case "weapon0":
                    case "weapon2":
                    case "weapon4":
                        weapon._data.enemySuffered.add(enemy);
                        enemy.weaponSuffered.add(weapon);
                        break;
                    case "weapon1":
                        playState.player.playerWeaponManager.hitEffect("weapon1", weapon, enemy);
                        weapon.kill();
                        break;
                    case "weapon3":
                        playState.player.playerWeaponManager.hitEffect("weapon3", weapon, enemy);
                        weapon.kill();
                        break;
                    case "weapon5":
                        playState.player.playerWeaponManager.hitEffect("weapon5", weapon, enemy);
                        weapon.kill();
                        break;
                }
                
                // Note: I added the player's ATK stat to damage
                this.hurt(enemy, weapon._data.damage + playState.player.stats.atk);

                
                if (enemy.HP <= 0 && !enemy.isDead) {
                    enemy.weaponSuffered.clear();
                    enemy.isDead = true;
                    enemy.animations.play("die");
                    playState.notifyEnemyKilled(enemy);
                    game.time.events.add(500, () => { enemy.kill(); }, null, null);
                }
            }, null, this);
        });

        // Check collisions with Dino's Child
        game.physics.arcade.overlap(playState.dinosChild.sprite, this.enemies, function (child, enemy) {
            if(enemy.isDead) return;
            playState.dinosChild.hurt(this.atk);
        }, null, this);

    }
}