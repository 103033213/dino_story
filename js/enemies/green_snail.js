class GreenSnail extends enemy {
    constructor() {
        super();
        this.type = "Green Snail";
        this.setupEnemy('Green Snail', 5, 'greenSnail');

        this.enemies.forEach(function(enemy){
            // I can't get callAll to work for some reason...
            enemy.body.setSize(35, 25, 0, 10);
        }, this);
        this.enemies.callAll('animations.add', 'animations', 'normal', [0, 1, 2, 3, 4, 3, 2, 1], 12, true);
        this.enemies.callAll('animations.add', 'animations', 'hit', [5], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'die', [5, 6, 7, 8, 9, 10, 11, 12, 13], 12, false);
    }
}