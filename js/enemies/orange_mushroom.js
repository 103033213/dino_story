class OrangeMushroom extends enemy {
    constructor() {
        super();
        this.type = "Orange Mushroom";
        this.setupEnemy('Orange Mushroom', 5, 'orangeMushroom', this);

        this.enemies.forEach(function(enemy){
            // I can't get callAll to work for some reason...
            enemy.body.setSize(40, 50, 0, 10);
        }, this);
        this.enemies.callAll('animations.add', 'animations', 'normal', [0, 1], 2, true);
        this.enemies.callAll('animations.add', 'animations', 'read_to_jump', [2], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'jumping', [3], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'landing', [4], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'hit', [5], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'die', [5, 6, 7, 8], 12, false);
    }
}