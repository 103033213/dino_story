class RedSnail extends enemy {
    constructor() {
        super();
        this.type = "Red Snail";
        this.setupEnemy('Red Snail', 5, 'redSnail', this);

        this.enemies.forEach(function (enemy) {
            // I can't get callAll to work for some reason...
            enemy.body.setSize(40, 30, 0, 3);
        }, this);
        this.enemies.callAll('animations.add', 'animations', 'normal', [0, 1, 2, 3, 2, 1], 12, true);
        this.enemies.callAll('animations.add', 'animations', 'hit', [4], 1, false);
        this.enemies.callAll('animations.add', 'animations', 'die', [4, 5, 6, 7], 12, false);
    }
}