class BluePotion extends BaseItem {
    constructor() {
        super();
        this.setupItem(5, 'bluePotion');

        this.emitter = game.add.emitter(0, 0, 100);

        this.emitter.makeParticles('mpParticle');
        this.emitter.gravity = -200;
        this.emitter.setAlpha(1, 0.1, 1000);
    }

    particleBurst(x, y) {
        this.emitter.x = x;
        this.emitter.y = y;
        this.emitter.start(true, 1000, 0, 5);
    }

    itemEffect(player) {
        playState.npcManager.notifyQuestNPC("Picked Up Blue Potion");
        this.particleBurst(player.x + 20, player.y + 30);
        playState.player.stats.currMP += 100;
    }

    itemEffectOnChild(dinosChild) {
        // Dino will get MP if Dino's Child touches the potion
        playState.npcManager.notifyQuestNPC("Picked Up Blue Potion");
        this.particleBurst(dinosChild.x + 20, dinosChild.y + 30);
        playState.player.stats.currMP += 100;
        if (playState.player.stats.currMP > playState.player.stats.maxMP) {
            playState.player.stats.currMP = playState.player.stats.maxMP;
        }
    }
}