class OrangePotion extends BaseItem {
    constructor() {
        super();
        this.setupItem(5, 'orangePotion');

        this.emitter = game.add.emitter(0, 0, 100);

        this.emitter.makeParticles('healParticle');
        this.emitter.gravity = -200;
        this.emitter.setAlpha(1, 0.1, 1000);
    }

    particleBurst(x, y) {
        this.emitter.x = x;
        this.emitter.y = y;
        this.emitter.start(true, 1000, 0, 5);
    }

    itemEffect(player) {
        playState.npcManager.notifyQuestNPC("Picked Up Orange Potion");
        this.particleBurst(player.x + 20, player.y + 30);
        playState.player.heal(150);
    }

    itemEffectOnChild(dinosChild) {
        // Dino's Child will heal 150 HP if it touches the potion
        playState.npcManager.notifyQuestNPC("Picked Up Orange Potion");
        this.particleBurst(dinosChild.x + 20, dinosChild.y + 30);
        playState.dinosChild.heal(150);
    }
}