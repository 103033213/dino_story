class RareCandy extends BaseItem {
    constructor() {
        super();
        this.setupItem(5, 'rareCandy');

        this.items.forEach(function(item){
            item.scale.setTo(0.08, 0.08);
        }, this);

        this.spawnLock = true; // when spawnLock==true, cannot spawn (because quest isn't active)
    }

    itemEffect(player) {
        playState.npcManager.notifyQuestNPC("Picked Up Rare Candy");
        playState.player.currEXP += 10; // Increase EXP when picking up a candy
    }

    itemEffectOnChild(dinosChild) {
        // Dino will get EXP if Dino's Child touches the candy
        playState.npcManager.notifyQuestNPC("Picked Up Rare Candy");
        playState.player.currEXP += 10;
    }
}