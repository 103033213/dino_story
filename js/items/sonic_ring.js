class SonicRing extends BaseItem {
    constructor() {
        super();
        this.setupRings(100);

        this.spawnLock = true; // when spawnLock==true, cannot spawn (because quest isn't active)
    }

    setupRings(quantity) {
        this.items.enableBody = true;
        this.items.physicsBodyType = Phaser.Physics.ARCADE;
        this.items.createMultiple(quantity, 'sonicRing');
        this.items.setAll('anchor.x', 0);
        this.items.setAll('anchor.y', 1);
        this.items.setAll('scale.x', 0.5);
        this.items.setAll('scale.y', 0.5);
        this.items.setAll('body.gravity.y', 1000);
        this.items.setAll('outOfBoundsKill', true);
        this.items.setAll('checkWorldBounds', true);
        this.items.forEach(function (item) {
            item.animations.add('normal', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 10, true);
            item.animations.play('normal');
        });
    }

    spawnRing(x) {
        const item = this.items.getFirstExists(false);
        if (item) {
            item.reset(x, GAME_HEIGHT - 80);
            item.body.velocity.x = -playState.groundManager.groundSpeed + game.rnd.between(-80, 0);
            item.body.velocity.y = -500;
        }
    }

    spawnRingBunch(amount, x) {
        if (this.spawnLock) {
            // stops items from spawning if they are quest items and the quest isn't active
            return;
        }
        for (let i = 0; i < amount; i++) {
            this.spawnRing(x);
        }
    }

    itemEffect(player) {
        playState.npcManager.notifyQuestNPC("Picked Up Sonic Ring");
    }

    itemEffectOnChild(dinosChild) {
        // Dino will get the rings if Dino's Child touches them
        playState.npcManager.notifyQuestNPC("Picked Up Sonic Ring");
    }
}