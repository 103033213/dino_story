class BaseNPC {
    constructor(manager, loadSprite, spriteName=loadSprite) {
        this.manager = manager;
        this.loadSprite = loadSprite;
        this.spriteName = spriteName;

        this.sprite = game.add.sprite(GAME_WIDTH, GAME_HEIGHT - 70, this.loadSprite);
        this.sprite.anchor.setTo(0, 1);
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.body.outOfBoundsKill = true;
        this.sprite.body.checkWorldBounds = true;

        this.questAccepted = false;
        this.questCompleted = false;
        this.questEnded = false;
        //
        this.dialogueData = game.cache.getJSON('dialogueData');
    }

    spawn() {
        this.sprite.reset(GAME_WIDTH, GAME_HEIGHT - 80);
        this.sprite.body.velocity.x = -playState.groundManager.groundSpeed;
    }

    update() {

        // Trigger dialogue when touching player
        game.physics.arcade.overlap(playState.player.sprite, this.sprite, function () {

            // First time meeting, accepting a quest
            if (!this.questAccepted) {
                // This boolean will prevent the overlap from triggering multiple times.
                this.questAccepted = true;
                console.log("Accepted quest: " + this.spriteName);
                // Add to acceptedQuestNPCs
                this.manager.acceptedQuestNPCs.push(this);
                // Remove from unlockedNPCs
                let index = this.manager.unlockedNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.unlockedNPCs.splice(index, 1);
                }
                // Add quest to UI
                playState.uiManager.addActiveQuest(this);
                // Unlock quest item spawn (so enemies can drop the quest item)
                this.manager.unlockItemSpawn(this);

                // TODO: NPC QuestGive dialogue
                //  You can use this.spriteName to differentiate which NPC is talking
                game.paused = true;

                this.frame = game.add.sprite(GAME_WIDTH / 2, GAME_HEIGHT / 2, 'dialogue');
                this.frame.anchor.setTo(0.5, 0.5);

                this.npcHead = game.add.sprite(GAME_WIDTH / 2-172, GAME_HEIGHT / 2-20, this.loadSprite);
                this.npcHead.anchor.setTo(0.5, 0.5);

                this.npcHeadName = game.add.bitmapText(GAME_WIDTH / 2-172, GAME_HEIGHT / 2+45 , 'silkscreen', this.dialogueData[this.spriteName]['Name'],12)
                this.npcHeadName.anchor.setTo(0.5, 0.5);

                this.npcText = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-75 , 'silkscreen', textParse(this.dialogueData[this.spriteName]['QuestGive'][1 + '-' + playState.npcManager.dialogueChoices[this.spriteName]]),24);
                this.npcText2 = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-50 , 'silkscreen', '',24);//for option(2)
                this.npcText3 = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-25 , 'silkscreen', '',24);//for option(3)

                playState.talkControls.npcName = this.spriteName;
                playState.talkControls.frame = this.frame;
                playState.talkControls.npcHead = this.npcHead;
                playState.talkControls.npcHeadName = this.npcHeadName;
                playState.talkControls.npcText = this.npcText;
                playState.talkControls.npcText2 = this.npcText2;
                playState.talkControls.npcText3 = this.npcText3;
                playState.talkControls.questState = 'QuestGive';


                // TODO: npc_dinono2.js overwrites the update function, so also add the callback there
                // TODO: npc_diyesyes.js overwrites the update function, so also add the callback there

            }

            // Second time meeting (should only happen after the quest is completed), handing in a completed quest
            if (!this.questEnded && this.questCompleted) {
                // This boolean will prevent the overlap from triggering multiple times.
                this.questEnded = true;
                console.log("Ended quest: " + this.spriteName);
                // Remove from completedQuestNPCs
                let index = this.manager.completedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.completedQuestNPCs.splice(index, 1);
                }
                // Remove quest from UI
                playState.uiManager.removeActiveQuest(this);

                // TODO: NPC QuestComplete dialogue
                //  You can use this.spriteName to differentiate which NPC is talking
                game.paused = true;

                this.frame = game.add.sprite(GAME_WIDTH / 2, GAME_HEIGHT / 2, 'dialogue');
                this.frame.anchor.setTo(0.5, 0.5);

                this.npcHead = game.add.sprite(GAME_WIDTH / 2-172, GAME_HEIGHT / 2-20, `${this.loadSprite}`);
                this.npcHead.anchor.setTo(0.5, 0.5);

                this.npcHeadName = game.add.bitmapText(GAME_WIDTH / 2-172, GAME_HEIGHT / 2+45 , 'silkscreen', this.dialogueData[`${this.spriteName}`]['Name'],12)
                this.npcHeadName.anchor.setTo(0.5, 0.5);

                this.npcText = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-75 , 'silkscreen', textParse(this.dialogueData[`${this.spriteName}`]['QuestComplete'][1+'-'+playState.npcManager.dialogueChoices[this.spriteName]]),24);
                this.npcText2 = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-50 , 'silkscreen', '',24);//for option(2)
                this.npcText3 = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-25 , 'silkscreen', '',24);//for option(3)

                playState.talkControls.npcName=this.spriteName;
                playState.talkControls.frame=this.frame;
                playState.talkControls.npcHead=this.npcHead;
                playState.talkControls.npcHeadName=this.npcHeadName;
                playState.talkControls.npcText=this.npcText;
                playState.talkControls.npcText2=this.npcText2;
                playState.talkControls.npcText3=this.npcText3;
                playState.talkControls.questState='QuestComplete';


                this.questReward();
                this.sprite.kill();
            }

        }, null, this);
    }
}
