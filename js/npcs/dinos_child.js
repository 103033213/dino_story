// DINO'S CHILD, UNLOCKED AFTER A QUEST (DINONO2)
class DinosChild {
    constructor() {
        // When Dino's Child is invulnerable, he cannot be hurt
        this.invulnerable = false;
        this.onGround = false;
        this.overlapDino = false;
        this.dead = false;
        this.sold = false;

        // Set up stats
        this.stats = {
            maxHP: 150, // full HP bar
            currHP: 150, // current HP
            jump: 350,
            speed: 200,
            acceleration: 2 // this is a multiplier
        };

        // Create and set up sprite
        this.sprite = game.add.sprite(32, 320, 'dino');
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.body.collideWorldBounds = true;
        this.sprite.body.gravity.y = 500; // lower gravity than Dino
        this.sprite.body.setSize(20, 35, 9, 10);
        this.sprite.scale.setTo(0.6, 0.6); // mini version of Dino
        this.sprite.kill(); // Dino's Child shouldn't exist until bringToLife() is called

        // Set up animations
        this.sprite.animations.add('run', [2, 3], 10, true);
        this.sprite.animations.add('jump', [1], 1, true);
        this.sprite.animations.add('idle', [0, 1], 5, true);

        // Set up damage display animation
        this.damageDisplay = game.add.bitmapText(0, 0, 'silkscreenRed', "damage", 24);
        this.damageDisplay.anchor.setTo(0.5, 0.5);
        this.damageDisplay.exists = false;
        this.damageDisplayBounce = game.add.tween(this.damageDisplay).to({y: this.sprite.y - 10}, 1000, Phaser.Easing.Cubic.Out);
        this.damageDisplayBounce.onComplete.add(function () {
            game.add.tween(this.damageDisplay).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        }, this);

        // Set up heal display animation
        this.healDisplay = game.add.bitmapText(0, 0, 'silkscreenGreen', "heal", 24);
        this.healDisplay.anchor.setTo(0.5, 0.5);
        this.healDisplay.exists = false;
        this.healDisplayBounce = game.add.tween(this.healDisplay).to({y: this.sprite.y - 10}, 1000, Phaser.Easing.Cubic.Out);
        this.healDisplayBounce.onComplete.add(function () {
            game.add.tween(this.healDisplay).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        }, this);

        // Set up gravestone animations
        this.grave = game.add.sprite(0, 0, 'grave');
        this.grave.anchor.setTo(0.5, 1); // bottom middle
        this.grave.exists = false;
        game.physics.enable(this.grave, Phaser.Physics.ARCADE);
        this.grave.animations.add('plop', [0, 1, 2, 3, 4, 5, 6], 12, false);
        this.grave.animations.add('still', [6], 1, true);
        this.graveDrop = game.add.tween(this.grave).to({y: GAME_HEIGHT - 80}, 1000, Phaser.Easing.Cubic.In);
        this.graveDrop.onComplete.add(function () {
            this.sprite.kill();
            game.physics.arcade.isPaused = false;
            this.grave.animations.play('plop');
            this.grave.body.velocity.x = -playState.groundManager.groundSpeed;
        }, this);
    }

    bringToLife() {
        // This function should be called only after the quest Dinono2 is ended
        this.sprite.reset(playState.player.sprite.x, playState.player.sprite.y);

        // Add UI elements
        playState.uiManager.childInfo.visible = true;
        playState.uiManager.childHPBar.visible = true;
        playState.uiManager.childHPText.visible = true;

        // Unlock Diyesyes
        playState.npcManager.unlockNPC('npcDiyesyes');
    }

    hurt(damage) {
        // Check if Dino's Child is currently invulnerable
        if (this.invulnerable) {
            return;
        }
        // Half the damage if Dino is currently protecting (overlapping) Dino's Child
        if (this.overlapDino) {
            damage /= 2;
        }
        // Show damage amount above Dino's Child
        this.damageDisplay.text = "HP -" + damage;
        this.damageDisplay.alpha = 1;
        this.damageDisplay.reset(this.sprite.x + 20, this.sprite.y - 10);
        this.damageDisplayBounce.start();
        console.log("Dino's Child took " + damage + " damage.");
        this.stats.currHP -= damage;
        if (this.stats.currHP < 0) {
            this.stats.currHP = 0;
        }
        this.invulnerable = true;
        // Check if Dino's Child is still alive after hit
        if (this.stats.currHP > 0) {
            this.sprite.alpha = 0.5; // Make Dino's Child transparent while invulnerable
            game.time.events.add(1000, function () {
                // Make Dino's Child vulnerable again after 1 seconds
                this.sprite.alpha = 1;
                this.invulnerable = false;
            }, this);
        }
        game.camera.shake(0.01, 300);
    }

    heal(healHP) {
        // Show heal amount above Dino's Child
        this.healDisplay.text = "HP +" + healHP;
        this.healDisplay.alpha = 1;
        this.healDisplay.reset(this.sprite.x + 20, this.sprite.y - 10);
        this.healDisplayBounce.start();
        console.log("Dino's Child healed " + healHP + " HP.");
        this.stats.currHP += healHP;
        if (this.stats.currHP > this.stats.maxHP) {
            this.stats.currHP = this.stats.maxHP;
        }
    }

    update() {
        // Death
        if (this.stats.currHP <= 0 && !this.dead) {
            this.dead = true;

            // Update dialogue for Diyesyes if child died before meeting him
            playState.npcManager.dialogueChoices.npcDiyesyes = 3;

            // Unlock Dinono2
            playState.npcManager.unlockNPC('npcDinono2');

            // Stop everything from moving
            game.physics.arcade.isPaused = true;

            // Drop the grave
            this.grave.exists = true;
            this.grave.reset(this.sprite.x + 10, 0);
            this.graveDrop.start();
            this.grave.animations.play('still');

            // Change visuals to be blood-red
            playState.bgManager.bgGradient.loadTexture('backgroundGradientDeath');
            playState.groundManager.visualGround.getChildAt(0).loadTexture('groundDeath');
            playState.groundManager.visualGround.getChildAt(1).loadTexture('groundDeath');
        }

        // Ground collision
        this.onGround = false;
        game.physics.arcade.collide(this.sprite, playState.groundManager.physicalGround, function () {
            this.onGround = true;
        }, null, this);

        // Overlapping with Dino
        this.overlapDino = false;
        game.physics.arcade.overlap(this.sprite, playState.player.sprite, function () {
            this.overlapDino = true;
        }, null, this);

        // Movement
        let distanceFromDino = playState.player.sprite.x - this.sprite.x;
        if (distanceFromDino < 0) {
            // Dino is on the left
            this.sprite.body.acceleration.x = this.stats.acceleration * distanceFromDino;
        }
        if (distanceFromDino > 0) {
            // Dino is on the right
            this.sprite.body.acceleration.x = this.stats.acceleration * distanceFromDino;
        }
        // Custom max velocity
        if (this.sprite.body.velocity.x > this.stats.speed) {
            this.sprite.body.velocity.x = this.stats.speed;
        } else if (this.sprite.body.velocity.x < -this.stats.speed) {
            this.sprite.body.velocity.x = -this.stats.speed;
        }

        if (this.overlapDino) {
            // Jump if Dino is overlapping
            this.sprite.body.velocity.y = -this.stats.jump;
        }

        // Animations
        if (this.onGround) {
            this.sprite.animations.play('run');
        } else {
            this.sprite.animations.play('jump');
        }

        // Safety checks
        if (this.stats.currHP > this.stats.maxHP) {
            this.stats.currHP = this.stats.maxHP;
        }
        if (this.stats.currHP < 0) {
            this.stats.currHP = 0;
        }
    }
}