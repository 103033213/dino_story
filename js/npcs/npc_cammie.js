class NPCCammie extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcCammie');

        //this.sprite.scale.setTo(0.6, 0.6);

        this.questProgress = 0;
        this.questGoal = 10;
        this.questTarget = 'BlueSnail';
    }

    questReward() {
        // Award EXP to player
        playState.player.currEXP += 40;
    }

    trackQuest(questEvent) {
        // Keeps track of the quest requirements and sets questCompleted to true once it's finished.
        if (questEvent === 'Killed Blue Snail') {
            this.questProgress++;
            if (this.questProgress >= this.questGoal) {
                this.questCompleted = true;
                console.log("Cammie's Quest COMPLETE!");
                // Add to completedQuestNPCs
                this.manager.completedQuestNPCs.push(this);
                // Remove from acceptedQuestNPCs
                let index = this.manager.acceptedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.acceptedQuestNPCs.splice(index, 1);
                }
            }
            playState.uiManager.trackActiveQuests();
        }
    }
}
