class NPCDinono2 extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcDinono', 'npcDinono2');

        this.questProgress = 0;
        this.questGoal = 0;
        this.questTarget = 'None';
    }

    questReward() {
        // TODO: kill Dino
    }

    trackQuest(questEvent) {
        // Shouldn't need to be used
    }

    // overwrite
    update() {

        // Trigger dialogue when touching player
        game.physics.arcade.overlap(playState.player.sprite, this.sprite, function () {

            // First time meeting, accepting a quest
            if (!this.questAccepted) {
                // This boolean will prevent the overlap from triggering multiple times.
                this.questAccepted = true;
                this.questCompleted = true;
                console.log("Ran into Dinono after killing child. Instant death.");
                // Remove from unlockedNPCs
                let index = this.manager.unlockedNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.unlockedNPCs.splice(index, 1);
                }

                // TODO: NPC QuestGive dialogue
                //  You can use this.spriteName to differentiate which NPC is talking
                game.paused = true;

                this.frame = game.add.sprite(GAME_WIDTH / 2, GAME_HEIGHT / 2, 'dialogue');
                this.frame.anchor.setTo(0.5, 0.5);

                this.npcHead = game.add.sprite(GAME_WIDTH / 2-172, GAME_HEIGHT / 2-20, `${this.loadSprite}`);
                this.npcHead.anchor.setTo(0.5, 0.5);

                this.npcHeadName = game.add.bitmapText(GAME_WIDTH / 2-172, GAME_HEIGHT / 2+45 , 'silkscreen', this.dialogueData[`${this.spriteName}`]['Name'],12)
                this.npcHeadName.anchor.setTo(0.5, 0.5);

                this.npcText = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-75 , 'silkscreen', textParse(this.dialogueData[`${this.spriteName}`]['QuestGive'][1+'-'+playState.npcManager.dialogueChoices[this.spriteName]]),24);
                this.npcText2 = game.add.bitmapText(GAME_WIDTH / 2-88, GAME_HEIGHT / 2-50 , 'silkscreen', '',24);//for option(2)

                playState.talkControls.npcName=this.spriteName;
                playState.talkControls.frame=this.frame;
                playState.talkControls.npcHead=this.npcHead;
                playState.talkControls.npcHeadName=this.npcHeadName;
                playState.talkControls.npcText=this.npcText;
                playState.talkControls.npcText2=this.npcText2;
                playState.talkControls.questState='QuestGive'


            }

            // Should trigger immediately after initial dialogue
            if (!this.questEnded && this.questCompleted) {
                this.questEnded = true;
                playState.player.hurt(99999);
            }
        }, null, this);
    }
}
