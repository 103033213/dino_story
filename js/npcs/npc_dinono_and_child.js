class NPCDinonoAndChild extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcDinonoAndChild');

        this.questProgress = 0;
        this.questGoal = 3;
        this.questTarget = 'BluePotion';
    }

    questReward() {
        // Award EXP to player
        playState.player.currEXP += 120;
        playState.dinosChild.bringToLife();
    }

    trackQuest(questEvent) {
        // Keeps track of the quest requirements and sets questCompleted to true once it's finished.
        if (questEvent === 'Picked Up Blue Potion') {
            this.questProgress++;
            if (this.questProgress >= this.questGoal) {
                this.questCompleted = true;
                console.log("Dinono's Quest COMPLETE!");
                // Add to completedQuestNPCs
                this.manager.completedQuestNPCs.push(this);
                // Remove from acceptedQuestNPCs
                let index = this.manager.acceptedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.acceptedQuestNPCs.splice(index, 1);
                }
            }
            playState.uiManager.trackActiveQuests();
        }
    }
}