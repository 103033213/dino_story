class NPCGuide extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcGuide');

        this.questProgress = 0;
        this.questGoal = 10;
        this.questTarget = 'GreenSnail';
    }

    questReward() {
        // Award EXP to player
        playState.player.currEXP += 30;
    }

    trackQuest(questEvent) {
        // Keeps track of the quest requirements and sets questCompleted to true once it's finished.
        if (questEvent === 'Killed Green Snail') {
            this.questProgress++;
            if (this.questProgress >= this.questGoal) {
                this.questCompleted = true;
                console.log("NPCGuide's Quest COMPLETE!");
                // Add to completedQuestNPCs
                this.manager.completedQuestNPCs.push(this);
                // Remove from acceptedQuestNPCs
                let index = this.manager.acceptedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.acceptedQuestNPCs.splice(index, 1);
                }
            }
            playState.uiManager.trackActiveQuests();
        }
    }
}