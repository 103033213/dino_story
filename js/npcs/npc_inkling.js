class NPCInkling extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcInkling');

        //this.sprite.scale.setTo(0.6, 0.6);

        this.questProgress = 0;
        this.questGoal = 3;
        this.questTarget = 'Skills';
    }

    questReward() {
        // Award EXP to player
        playState.player.currEXP += 30;
    }

    trackQuest(questEvent) {
        // Keeps track of the quest requirements and sets questCompleted to true once it's finished.
        if (questEvent === 'Used Skill') {
            this.questProgress++;
            if (this.questProgress >= this.questGoal) {
                this.questCompleted = true;
                console.log("Inkling's Quest COMPLETE!");
                // Add to completedQuestNPCs
                this.manager.completedQuestNPCs.push(this);
                // Remove from acceptedQuestNPCs
                let index = this.manager.acceptedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.acceptedQuestNPCs.splice(index, 1);
                }
            }
            playState.uiManager.trackActiveQuests();
        }
    }
}
