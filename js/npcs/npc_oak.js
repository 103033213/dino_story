class NPCOak extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcOak');

        this.questProgress = 0;
        this.questGoal = 5;
        this.questTarget = 'RareCandy';
    }

    questReward() {
        // Moved to talk_controls.js to fix 判定時間 bug
        // Unlock Inkling quest
        this.manager.unlockNPC('npcInkling');
    }

    trackQuest(questEvent) {
        // Keeps track of the quest requirements and sets questCompleted to true once it's finished.
        if (questEvent === 'Picked Up Rare Candy') {
            this.questProgress++;
            if (this.questProgress >= this.questGoal) {
                this.questCompleted = true;
                console.log("Oak's Quest COMPLETE!");
                // Add to completedQuestNPCs
                this.manager.completedQuestNPCs.push(this);
                // Remove from acceptedQuestNPCs
                let index = this.manager.acceptedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.acceptedQuestNPCs.splice(index, 1);
                }
                // Lock quest item spawn (so enemies can't drop the quest item)
                this.manager.lockItemSpawn(this);
            }
            // Update quests UI
            playState.uiManager.trackActiveQuests();
        }
    }
}