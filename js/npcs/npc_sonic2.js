class NPCSonic2 extends BaseNPC {
    constructor(manager) {
        super(manager, 'npcSonic', 'npcSonic2');

        //this.sprite.scale.setTo(2, 2);

        this.questProgress = 0;
        this.questGoal = 200;
        this.questTarget = 'SonicRing';
    }

    questReward() {
        // Change job to dialogue choice
        let choice = this.manager.dialogueChoices.npcSonic2;
        if (choice === 1) {
            // upgrade jump level
            let JUMP = playState.player.stats.jump_stats;
            JUMP.jump_height = JUMP.jump_height + (JUMP.jump_height_MAX - JUMP.jump_height_min) / JUMP.jump_level_MAX;
            JUMP.jump_level = JUMP.jump_level + 1;
        } else if (choice === 2) {
            // upgrade speed level
            let SPEED = playState.player.stats.speed_stats;
            SPEED.speed = SPEED.speed + (SPEED.speed_MAX - SPEED.speed_min) / SPEED.speed_level_MAX;
            SPEED.speed_level = SPEED.speed_level + 1;
        } else {
            console.log("Speed/Jump upgrade failed, dialogue choice = ", choice);
        }

        // Unlock Sonic3 quest
        this.manager.unlockNPC('npcSonic3');
    }

    trackQuest(questEvent) {
        // Keeps track of the quest requirements and sets questCompleted to true once it's finished.
        if (questEvent === 'Picked Up Sonic Ring') {
            this.questProgress++;
            if (this.questProgress >= this.questGoal) {
                this.questCompleted = true;
                console.log("Sonic's Quest COMPLETE!");
                // Add to completedQuestNPCs
                this.manager.completedQuestNPCs.push(this);
                // Remove from acceptedQuestNPCs
                let index = this.manager.acceptedQuestNPCs.indexOf(this);
                if (index > -1) {
                    this.manager.acceptedQuestNPCs.splice(index, 1);
                }
                // Lock quest item spawn (so enemies can't drop the quest item)
                this.manager.lockItemSpawn(this);
            }
            // Update quests UI
            playState.uiManager.trackActiveQuests();
        }
    }
}
