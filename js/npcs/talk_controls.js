//dialogue control

class TalkControls {
    constructor() {
        this.selectDown = false;
        this.selectUp = false;
        this.select = false;
        this.talkPointer = 1;
        this.npcName = null;
        this.npcText = null;
        this.npcText2 = null;
        this.npcText3 = null;
        this.npcHead = null;
        this.npcHeadName = null;
        this.frame = null;
        this.questState = null;//give or completed

        this.dialogueData = game.cache.getJSON('dialogueData');

        this.selectDownBtn = game.input.keyboard.addKey(Phaser.KeyCode.DOWN);
        this.selectDownBtn.onDown.add(npcOptionDown, null, this.npcText, this);

        this.selectUpBtn = game.input.keyboard.addKey(Phaser.KeyCode.UP);
        this.selectUpBtn.onDown.add(npcOptionUp, null, this.npcText, this);

        this.selectBtn = game.input.keyboard.addKey(Phaser.KeyCode.ENTER);
        this.selectBtn.onDown.add(textChange, null, this.npcText, this);
    }

    update() {
        // this.selectDown = this.selectDownBtn.isDown;
        // this.selectUp = this.selectUpBtn.isDown;
        // this.select = this.selectBtn.isDown;
    }

}

function textChange(a, b) {
    if (b.npcText3.text != '') {
      if (b.npcText.tint === 0xffeeee) {
          playState.npcManager.dialogueChoices[b.npcName] = 1;
      } else if(b.npcText2.tint === 0xffeeee){
          playState.npcManager.dialogueChoices[b.npcName] = 2;
      } else if(b.npcText3.tint === 0xffeeee){
          playState.npcManager.dialogueChoices[b.npcName] = 3;
      }
      b.npcText2.tint = 0xffffff;
      b.npcText2.text = '';
      b.npcText3.tint = 0xffffff;
      b.npcText3.text = '';
      b.npcText.tint = 0xffffff;
    }else if (b.npcText2.text != '') {
        if (b.npcText.tint === 0xffeeee) {
            playState.npcManager.dialogueChoices[b.npcName] = 1;
        } else {
            playState.npcManager.dialogueChoices[b.npcName] = 2;
        }
        b.npcText2.tint = 0xffffff;
        b.npcText2.text = '';
        b.npcText.tint = 0xffffff;
    }
    if (game.paused) {
        b.talkPointer++;
        if (b.dialogueData[b.npcName][b.questState][b.talkPointer + '-' + playState.npcManager.dialogueChoices[b.npcName]] === 0) {
            b.talkPointer = 1;
            b.frame.destroy();
            b.npcHead.destroy();
            b.npcHeadName.destroy();
            b.npcText.destroy();
            //console.log(b.npcText);
            console.log('Ending dialogue for ' + b.npcName);

            // Bug fix for 判定時間
            if (b.npcName === 'npcDiyesyes') {
                // 賣小恐龍
                let diyesyesQuest = playState.npcManager.npcDiyesyes;
                if (!diyesyesQuest.questEnded && diyesyesQuest.questCompleted) {
                    diyesyesQuest.questEnded = true;
                    if (playState.npcManager.dialogueChoices.npcDiyesyes === 1) {
                        playState.dinosChild.sold = true;
                        playState.dinosChild.sprite.kill();
                        playState.player.spitMoney();
                    }
                }
            } else if (b.npcName === 'npcOak') {
                // 大木博士：轉職
                let choice = playState.npcManager.dialogueChoices.npcOak;
                if (choice === 1) {
                    // 轉職法師
                    console.log("Dino is now a MAGICIAN");
                    playState.player.playerJobManager.changeJob('MAGICIAN');
                } else if (choice === 2) {
                    // 轉職戰士
                    console.log("Dino is now a WARRIOR");
                    playState.player.playerJobManager.changeJob('WARRIOR');
                } else if(choice === 3){
                    // 轉職德魯衣
                    console.log("Dino is now a DRUID");
                    playState.player.playerJobManager.changeJob('DRUID');
                }else {
                    console.log("轉職失敗, dialogue choice = ", choice);
                }
            }

            game.paused = false;

        } else if (Array.isArray(b.dialogueData[b.npcName][b.questState][b.talkPointer + '-' + playState.npcManager.dialogueChoices[b.npcName]])) {
            b.npcText.text = b.dialogueData[b.npcName][b.questState][b.talkPointer + '-' + playState.npcManager.dialogueChoices[b.npcName]][0];
            b.npcText.tint = 0xffeeee;
            b.npcText2.text = b.dialogueData[b.npcName][b.questState][b.talkPointer + '-' + playState.npcManager.dialogueChoices[b.npcName]][1];
            b.npcText2.tint = 0x888888;
            if (b.npcName === 'npcOak'){
              b.npcText3.text = b.dialogueData[b.npcName][b.questState][b.talkPointer + '-' + playState.npcManager.dialogueChoices[b.npcName]][2];
              b.npcText3.tint = 0x888888;
            }

        } else {
            b.npcText.text = textParse(b.dialogueData[b.npcName][b.questState][b.talkPointer + '-' + playState.npcManager.dialogueChoices[b.npcName]]);
        }
        console.log(b.talkPointer + '-' + playState.npcManager.dialogueChoices[b.npcName]);
    }
}

function textParse(text) {
    text = text.split(' ');
    //console.log(text);
    let length = text.length;
    let point = 1;
    let parsedText = '';
    let num = 0;
    parsedText += ' ' + text[0];
    //console.log(length);
    while (length > point) {
        if (parsedText.length + text[point].length + 1 - num < 21) {
            //console.log(parsedText.length + text[point].length + 1-num);
            parsedText += ' ' + text[point];
            point++;
        } else {
            //console.log('change line');
            parsedText += '\n';
            num = parsedText.length;
        }
    }
    return parsedText;
}

function npcOptionDown(a, b) {
    if (b.npcText.tint === 0xffeeee) {
      b.npcText.tint =0x888888;
      b.npcText2.tint =0xffeeee;
      //console.log(1);
    }
    else if (b.npcText2.tint === 0xffeeee) {
      b.npcText2.tint =0x888888;
      b.npcText3.tint =0xffeeee;
      //console.log(2);
    }
    else if (b.npcText3.tint === 0xffeeee) {
      b.npcText3.tint =0x888888;
      b.npcText.tint =0xffeeee;
      //console.log(3);
    }
}
function npcOptionUp(a, b) {
    if (b.npcText.tint === 0xffeeee) {
      b.npcText.tint =0x888888;
      b.npcText3.tint =0xffeeee;
      //console.log(1);
    }
    else if (b.npcText2.tint === 0xffeeee) {
      b.npcText2.tint =0x888888;
      b.npcText.tint =0xffeeee;
      //console.log(2);
    }
    else if (b.npcText3.tint === 0xffeeee) {
      b.npcText3.tint =0x888888;
      b.npcText2.tint =0xffeeee;
      //console.log(3);
    }
}
