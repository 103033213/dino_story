// TAKES USER INPUT, WHICH THEN CONTROLS THE PLAYER CHARACTER

class Controls {
    constructor() {
        this.moveLeft = false;
        this.moveRight = false;
        this.jump = false;
        this.attack = false;
        this.changeWeapon = false;
        this.special1 = false;
        this.special2 = false;

        this.leftBtn = game.input.keyboard.addKey(Phaser.KeyCode.LEFT);
        this.rightBtn = game.input.keyboard.addKey(Phaser.KeyCode.RIGHT);
        this.jumpBtn = game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.attackBtn = game.input.keyboard.addKey(Phaser.KeyCode.Z);
        this.changeWeaponBtn = game.input.keyboard.addKey(Phaser.KeyCode.Q);
        this.special1Btn = game.input.keyboard.addKey(Phaser.KeyCode.X);
        this.special2Btn = game.input.keyboard.addKey(Phaser.KeyCode.C);
        this.special3Btn = game.input.keyboard.addKey(Phaser.KeyCode.A);
    }

    update() {
        this.moveLeft = this.leftBtn.isDown;
        this.moveRight = this.rightBtn.isDown;
        this.jump = this.jumpBtn.isDown;
        this.attack = this.attackBtn.isDown;
        this.changeWeapon = this.changeWeaponBtn.justDown;
        this.special1 = this.special1Btn.isDown;
        this.special2 = this.special2Btn.isDown;
        this.special3 = this.special3Btn.isDown;
    }

}