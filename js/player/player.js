// THE PLAYER CHARACTER
class Player {
    constructor() {
        // When Dino is invulnerable, he cannot be hurt
        this.invulnerable = false;
        this.playerOnGround = false;

        // Set up player stats (level 1)
        this.stats = {
            maxHP: 100, // full HP bar
            currHP: 100, // current HP
            maxMP: 50, // full MP bar
            currMP: 50, // current MP
            atk: 1, // attack
            def: 1, // defense
            dex: 1, // dexterity
            mag: 1, // magic
            speed: 200,
            alivetime: 0,// alivetime
            //for jump
            jump_stats: {
                jump_height_MAX: 650,
                jump_height_min: 350,
                jump_height: 350,
                jump_level_MAX: 3,
                jump_level: 0,
                jump_increase_time: 2
            },
            //for speed
            speed_stats: {
                speed_MAX: 500,
                speed_min: 200,
                speed: 200,
                speed_level_MAX: 3,
                speed_level: 0,
                speed_increase_time: 2
            }

        };

        // Set up player level
        this.prevLvlEXP = 0; // Storing this data makes the EXP bar from UiManager run faster
        this.nextLvlEXP = 0; // EXP to get to next level; will be overwritten by PlayerLvlManager
        this.currEXP = 0; // current EXP
        this.playerLvlManager = new PlayerLvlManager(this); // manages the Leveling System for the player (a.k.a. 升等)
        // Initialize player stats at level 1
        this.currLvl = 0;
        this.playerLvlManager.levelUp();

        // Set up player job
        this.currJob = 'NEWBIE'; // current job "新手"
        this.playerJobManager = new PlayerJobManager(this); // manages the Job System for the player (a.k.a. 職業)

        // Set up player weapon
        this.currWeapon = 0; // current weapon  = 0, 1, 2, 3
        this.playerWeaponManager = new PlayerWeaponManager(this); // manages the Weapon System for the player (a.k.a. 武器)

        // Create and set up player character: Dino
        this.sprite = game.add.sprite(32, 320, 'dino');
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.body.collideWorldBounds = true;
        this.sprite.body.gravity.y = 1000;
        this.sprite.body.setSize(20, 35, 9, 10);

        // Set up Dino's animations
        this.sprite.animations.add('run', [2, 3], 10, true);
        this.sprite.animations.add('jump', [1], 1, true);
        this.sprite.animations.add('idle', [0, 1], 5, true);

        // Set up gravestone animations
        this.grave = game.add.sprite(0, 0, 'grave');
        this.grave.anchor.setTo(0.5, 1); // bottom middle
        this.grave.exists = false;
        this.grave.animations.add('plop', [0, 1, 2, 3, 4, 5, 6], 12, false);
        this.grave.animations.add('still', [6], 1, true);
        this.graveDrop = game.add.tween(this.grave).to({y: GAME_HEIGHT - 80}, 1000, Phaser.Easing.Cubic.In);
        this.graveDrop.onComplete.add(function () {
            this.sprite.kill();
            this.grave.animations.play('plop');
        }, this);

        // Set up lvl-up animation
        this.lvlUpExplosion = game.add.sprite(0, 0, 'lvlUpExplosion');
        this.lvlUpExplosion.anchor.setTo(0.5, 1); // bottom middle
        game.physics.enable(this.lvlUpExplosion, Phaser.Physics.ARCADE);
        this.lvlUpExplosion.body.velocity.x = -playState.groundManager.groundSpeed;
        this.lvlUpExplosion.exists = false;
        this.lvlUpExplosionAnim = this.lvlUpExplosion.animations.add('explode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 12, false);

        // Set up damage display animation
        this.damageDisplay = game.add.bitmapText(0, 0, 'silkscreenRed', "damage", 24);
        this.damageDisplay.anchor.setTo(0.5, 0.5);
        this.damageDisplay.exists = false;
        this.damageDisplayBounce = game.add.tween(this.damageDisplay).to({y: this.sprite.y - 10}, 1000, Phaser.Easing.Cubic.Out);
        this.damageDisplayBounce.onComplete.add(function () {
            game.add.tween(this.damageDisplay).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        }, this);

        // Set up heal display animation
        this.healDisplay = game.add.bitmapText(0, 0, 'silkscreenGreen', "heal", 24);
        this.healDisplay.anchor.setTo(0.5, 0.5);
        this.healDisplay.exists = false;
        this.healDisplayBounce = game.add.tween(this.healDisplay).to({y: this.sprite.y - 10}, 1000, Phaser.Easing.Cubic.Out);
        this.healDisplayBounce.onComplete.add(function () {
            game.add.tween(this.healDisplay).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        }, this);

        // Set up heal display animation
        this.lvlUpDisplay = game.add.bitmapText(0, 0, 'silkscreen', "Lvl Up!", 30);
        this.lvlUpDisplay.anchor.setTo(0.5, 0.5);
        this.lvlUpDisplay.exists = false;
        this.lvlUpDisplayBounce = game.add.tween(this.lvlUpDisplay).to({y: this.sprite.y - 10}, 1000, Phaser.Easing.Cubic.Out);
        this.lvlUpDisplayBounce.onComplete.add(function () {
            game.add.tween(this.lvlUpDisplay).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        }, this);

        // Money emitter for Diyesyes's quest reward
        this.moneyEmitter = game.add.emitter(0, 0, 30);
        this.moneyEmitter.makeParticles('coin');
        this.moneyEmitter.gravity = 1000;
        this.moneyEmitter.setYSpeed(-100, -500);
        this.moneyEmitter.forEach(function (singleParticle) {
            singleParticle.animations.add('spin', [0, 1, 2, 3], 12, true);
            singleParticle.animations.play('spin');
        });
    }

    spitMoney() {
        // Triggers the money emitter for Diyesyes's quest reward
        console.log("WE'VE HIT GOLD, BOYS!!!");
        this.moneyEmitter.start(false, 1000, 100);
    }

    hurt(damage) {
        // Check if Dino is currently invulnerable
        if (this.invulnerable) {
            return;
        }
        // Show damage amount above Dino
        this.damageDisplay.text = "HP -" + damage;
        this.damageDisplay.alpha = 1;
        this.damageDisplay.reset(this.sprite.x + 20, this.sprite.y - 10);
        this.damageDisplayBounce.start();
        // Hurt Dino and make him temporarily invulnerable
        console.log("Dino took " + damage + " damage.");
        this.stats.currHP -= damage;
        if (this.stats.currHP < 0) {
            this.stats.currHP = 0;
        }
        this.enterInvulnerable();
        game.camera.shake(0.01, 300);
    }
    enterInvulnerable(){
        this.invulnerable = true;
        // Check if Dino is still alive after hit
        if (this.stats.currHP > 0) {
            this.sprite.alpha = 0.5; // Make Dino transparent while invulnerable
            game.time.events.add(1000, function () {
                // Make Dino vulnerable again after 1 seconds
                this.sprite.alpha = 1;
                this.invulnerable = false;
            }, this);
        }
    }

    heal(healHP) {
        // Show heal amount above Dino
        this.healDisplay.text = "HP +" + healHP;
        this.healDisplay.alpha = 1;
        this.healDisplay.reset(this.sprite.x + 20, this.sprite.y - 10);
        this.healDisplayBounce.start();
        console.log("Dino healed " + healHP + " HP.");
        this.stats.currHP += healHP;
        if (this.stats.currHP > this.stats.maxHP) {
            this.stats.currHP = this.stats.maxHP;
        }
    }

    attack(skill) {
        if (this.playerWeaponManager.cooldown > 0) return;

        let [target, distance] = this.findClosestEnemy();

        if (this.currWeapon === 0 && this.currJob === "NEWBIE") {
            if (!skill && this.playerWeaponManager.inRange("weapon0", target)) {
                this.playerWeaponManager.release("weapon0", target);
                this.playerWeaponManager.cooldown = 25;
            } else if (skill) {
                if(this.stats.currMP >= 10){
                    this.playerWeaponManager.release("weapon1", target);
                    this.playerWeaponManager.cooldown = 25;
                    this.stats.currMP -= 10;
                }else{
                    console.log("lack of MP");
                }
            }
        }
        else if(this.currWeapon === 1 || this.currJob === "DRUID"){  // ???
            if (!skill && this.playerWeaponManager.inRange("weapon2", target)) {                
                this.playerWeaponManager.release("weapon2", target);
                this.playerWeaponManager.cooldown = 25;
            } else if (skill) {
                if(this.stats.currMP >= 30){
                    this.playerWeaponManager.release("weapon3", target);
                    this.playerWeaponManager.cooldown = 25;
                    this.stats.currMP -= 30;
                }else{
                    console.log("lack of MP");
                }
            }

        }
        else if (this.currWeapon === 2 || this.currJob === "WARRIOR"){  // ???
            if (!skill && this.playerWeaponManager.inRange("weapon4", target)) {
                this.playerWeaponManager.release("weapon4", target);
                this.playerWeaponManager.cooldown = 25;
            } else if (skill) {
                if(this.stats.currHP >= 5){
                    this.playerWeaponManager.release("weapon5", target);
                    this.playerWeaponManager.cooldown = 25;
                    this.stats.currHP -= 5;
                }else{
                    console.log("lack of HP");
                }
            }
        }
        else if(this.currWeapon === 3 || this.currJob === "MAGICIAN"){  // ????
            if (!skill && this.playerWeaponManager.inRange("weapon6", target)) {
                if(this.stats.currMP > 200){
                    this.playerWeaponManager.release("weapon6", target);
                    this.playerWeaponManager.cooldown = 25;
                    this.stats.currMP -= 200;
                }else{
                    console.log("lack of MP");
                }
            } else if (skill) {
                this.playerWeaponManager.release("weapon5", target);
                this.playerWeaponManager.cooldown = 25;
            }
        }
    }

    findClosestEnemy() {
        let target = null;
        let distance = 99999;
        playState.enemyManager.allEnemies.forEach((enemyGroup) => {
            enemyGroup.enemies.forEachAlive((enemy) => {
                let d = Phaser.Math.distance(this.sprite.body.x, this.sprite.y, enemy.x, enemy.y);
                if (distance > d) {
                    distance = d;
                    target = enemy;
                }
            })
        });
        return [target, distance];
    }

    update() {
        // Death
        if (this.stats.currHP <= 0 && playState.stateTag === STATES.RUNNING) {
            playState.stateTag = STATES.GAME_OVER;
            // Stop everything from moving
            game.physics.arcade.isPaused = true;
            // Drop the grave
            this.grave.exists = true;
            this.grave.reset(this.sprite.x + 20, 0);
            this.graveDrop.start();
            this.grave.animations.play('still');
        }

        // Level Up
        if (this.currEXP >= this.nextLvlEXP) {
            this.playerLvlManager.levelUp();

            this.lvlUpDisplay.alpha = 1;
            this.lvlUpDisplay.reset(this.sprite.x + 20, this.sprite.y - 10);
            this.lvlUpDisplayBounce.start();

            this.lvlUpExplosion.exists = true;
            this.lvlUpExplosion.reset(this.sprite.x + 20, this.sprite.y + this.sprite.height);
            this.lvlUpExplosion.animations.play('explode');
            this.lvlUpExplosionAnim.onComplete.add(function () {
                this.lvlUpExplosion.exists = false;
            }, this);

            console.log("Dino leveled up.");
        }

        // Ground collision
        this.playerOnGround = false;
        game.physics.arcade.collide(this.sprite, playState.groundManager.physicalGround, function () {
            this.playerOnGround = true;
        }, null, this);

        // Movement
        this.sprite.body.velocity.x = 0;
        if (playState.controls.moveLeft) {
            this.sprite.body.velocity.x -= this.stats.speed_stats.speed + playState.groundManager.groundSpeed;
        }
        if (playState.controls.moveRight) {
            this.sprite.body.velocity.x += this.stats.speed_stats.speed;
        }
        if (playState.controls.jump && this.playerOnGround) {
            this.sprite.body.velocity.y = -this.stats.jump_stats.jump_height;
        }

        // Animations
        if (this.playerOnGround) {
            this.sprite.animations.play('run');
        } else {
            this.sprite.animations.play('jump');
        }

        if (playState.controls.attack) {
            this.attack(false);
        }
        if (playState.controls.special1) {
            this.attack(true);
        }
        if (playState.controls.special2) {
            this.currEXP += 5;
        }
        if (playState.controls.changeWeapon){
            this.currWeapon = (this.currWeapon + 1) % 4;
            console.log("weapon is", this.currWeapon);
        }

        // Money emitter update
        this.moneyEmitter.x = this.sprite.x + 15;
        this.moneyEmitter.y = this.sprite.y + 15;

        // Safety checks
        if (this.stats.currHP > this.stats.maxHP) {
            this.stats.currHP = this.stats.maxHP;
        }
        if (this.stats.currMP > this.stats.maxMP) {
            this.stats.currMP = this.stats.maxMP;
        }
        if (this.stats.currHP < 0) {
            this.stats.currHP = 0;
        }
        if (this.stats.currMP < 0) {
            this.stats.currMP = 0;
        }

        // Speed and Jump level are now bound to NPC Sonic
        // //alive time update
        // this.alivetime = game.time.totalElapsedSeconds();
        // var JUMP = this.stats.jump_stats;
        // if (this.alivetime > JUMP.jump_increase_time * (JUMP.jump_level + 1) && JUMP.jump_level < JUMP.jump_level_MAX) {
        //     console.log("jump higher");
        //     JUMP.jump_height = JUMP.jump_height + (JUMP.jump_height_MAX - JUMP.jump_height_min) / JUMP.jump_level_MAX
        //     JUMP.jump_level = JUMP.jump_level + 1;
        // }
        // var SPEED = this.stats.speed_stats;
        // if (this.alivetime > SPEED.speed_increase_time * (SPEED.speed_level + 1) && SPEED.speed_level < SPEED.speed_level_MAX) {
        //     console.log("speed up");
        //     SPEED.speed = SPEED.speed + (SPEED.speed_MAX - SPEED.speed_min) / SPEED.speed_level_MAX
        //     SPEED.speed_level = SPEED.speed_level + 1;
        // }

        this.playerWeaponManager.update();
    }
}