// Manages the Job System for the player (a.k.a. 職業)
// NOTE: I think we should do this later. Making enemies and combat is more important for now.

class PlayerJobManager {
    constructor(player) {
        this.player = player;

        this.jobs = ['NEWBIE', 'MAGICIAN', 'WARRIOR', 'DRUID'];
        // NEWBIE — dude
        // MAGICIAN — ranged, high dexterity
        // WARRIOR — high HP and spend HP to cause damadge
        // DRUID — more XP
    }

    changeJob(newjob){
        playState.player.stats.ultimate_cooldown = 5;
        switch (newjob) {
            case 'MAGICIAN':
                console.log("new job is MAGICIAN");
                playState.player.currJob = this.jobs[1];
                playState.player.stats.maxMP += 500;
                break;  
            case 'WARRIOR':
                console.log("new job is WARRIOR");
                playState.player.currJob = this.jobs[2];
                playState.player.stats.maxHP += 500;
                playState.player.stats.maxMP = 0;
                break;
            case 'DRUID':
                console.log("new job is DRUID");
                playState.player.currJob = this.jobs[3];
                playState.player.stats.maxHP += 200;
                playState.player.stats.maxMP += 200;
                playState.player.currEXP += playState.player.nextLvlEXP;
                break;
            default:
                console.log("no this job");
        }
    }
}