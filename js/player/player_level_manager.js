// Manages the Leveling System for the player (a.k.a. 升等)

class PlayerLvlManager {
    constructor(player) {
        this.player = player;
        this.lvlData = game.cache.getJSON('playerLvlData');
    }

    levelUp() {
        this.player.prevLvlEXP = this.player.nextLvlEXP;
        this.player.currLvl++;
        const lvlUpData = this.lvlData[this.player.currLvl];
        this.player.nextLvlEXP = lvlUpData.nextLvlEXP;
        this.player.stats.maxHP = lvlUpData.maxHP;
        this.player.stats.currHP = lvlUpData.maxHP;
        this.player.stats.maxMP = lvlUpData.maxMP;
        this.player.stats.currMP = lvlUpData.maxMP;
        this.player.stats.atk = lvlUpData.atk;
        this.player.stats.def = lvlUpData.def;
        this.player.stats.dex = lvlUpData.dex;
        this.player.stats.mag = lvlUpData.mag;
        // links to EnemyManager to update spawn timers
        playState.enemyManager.updateLevel(this.player.currLvl);
        // links to NPCManager to update available quests
        playState.npcManager.updateLevel(this.player.currLvl);
    }
}