// PRELOAD ALL ASSETS & DISPLAY LOADING PROGRESS

let loadState = {
    progressText: null,
    fadeOutTime: 1000,

    preload: function () {
        game.stage.backgroundColor = '#000000';
        // Text used for displaying loading progress needs to be loaded first
        game.load.bitmapFont('silkscreen', 'font/silkscreen/silkscreen.png', 'font/silkscreen/silkscreen.fnt');
    },

    create: function () {
        this.progressText = game.add.bitmapText(GAME_WIDTH/2, GAME_HEIGHT/2, 'silkscreen', 'Loading... 0%', 20);
        this.progressText.anchor.setTo(0.5, 0.5);

        game.load.onFileComplete.add(this.fileComplete, this);
        game.load.onLoadComplete.add(this.loadComplete, this);

        // TODO: ↓ ↓ ↓ LOAD ALL ASSETS HERE ↓ ↓ ↓ //
        // GroundManager
        game.load.image('ground', 'img/ground/ground.png');
        game.load.image('groundDeath', 'img/ground/ground_death.png');
        game.load.image('physicalGround', 'img/ground/physical_ground.png');
        // BgManager
        game.load.image('backgroundGradient', 'img/background/background_gradient.png');
        game.load.image('backgroundGradientDeath', 'img/background/background_gradient_death.png');
        game.load.image('cloud1', 'img/background/cloud1.png');
        game.load.image('cloud2', 'img/background/cloud2.png');
        game.load.image('cloud3', 'img/background/cloud3.png');
        game.load.image('cloud4', 'img/background/cloud4.png');
        game.load.image('cloud5', 'img/background/cloud5.png');
        game.load.image('farBg1', 'img/background/far_bg1.png');
        game.load.image('farBg2', 'img/background/far_bg2.png');
        game.load.image('nearBg1', 'img/background/near_bg1.png');
        game.load.image('nearBg2', 'img/background/near_bg2.png');
        game.load.image('nearBg3', 'img/background/near_bg3.png');
        game.load.image('nearBg4', 'img/background/near_bg4.png');
        // PlayState
        game.load.image('gameOverGradient', 'img/play_state/game_over_gradient.png');
        // Player
        game.load.spritesheet('dino', 'img/player/dino_spritesheet.png', 44, 47);
        game.load.spritesheet('lvlUpExplosion', 'img/player/lvl_up_explosion.png', 64, 64);
        game.load.atlasJSONHash('grave', 'img/player/grave.png', 'img/player/grave.json');
        game.load.atlasJSONHash('coin', 'img/player/coin.png', 'img/player/coin.json');
        game.load.bitmapFont('silkscreenRed', 'font/silkscreenRed/silkscreenRed.png', 'font/silkscreenRed/silkscreenRed.fnt');
        game.load.bitmapFont('silkscreenGreen', 'font/silkscreenGreen/silkscreenGreen.png', 'font/silkscreenGreen/silkscreenGreen.fnt');
        // PlayerLvlManager
        game.load.json('playerLvlData', 'data/player_level_data.json');
        // WeaponManager
        game.load.json('playerWeaponData', 'data/player_weapon_data.json');
        game.load.spritesheet('weapon0', 'img/player/weapon0.png', 96, 96);
        game.load.spritesheet('weapon1', 'img/player/weapon1.png', 36, 36);
        game.load.spritesheet('weapon1-hit', 'img/player/weapon1-hit.png', 80, 80);
        game.load.spritesheet('weapon2', 'img/player/weapon2.png', 100, 100);
        game.load.spritesheet('weapon3', 'img/player/weapon3.png', 32, 16);
        game.load.spritesheet('weapon3-hit', 'img/player/weapon3-hit.png', 100, 50);
        game.load.spritesheet('weapon4', 'img/player/weapon4.png', 180, 180);
        game.load.spritesheet('weapon5', 'img/player/weapon5.png', 60, 30);
        game.load.spritesheet('weapon5-hit', 'img/player/weapon5-hit.png', 120, 120);
        game.load.spritesheet('weapon6', 'img/player/weapon6.png', 880, 280);
        // UiManager
        game.load.image('uiPlayerInfo', 'img/ui/ui_player_info.png');
        game.load.image('uiHPBar', 'img/ui/ui_hp_bar.png');
        game.load.image('uiMPBar', 'img/ui/ui_mp_bar.png');
        game.load.image('uiEXPInfo', 'img/ui/ui_exp_info.png');
        game.load.image('uiEXPBar', 'img/ui/ui_exp_bar.png');
        game.load.image('uiQuestsBg', 'img/ui/ui_quests_bg.png');
        game.load.image('uiChildInfo', 'img/ui/ui_child_info.png');
        game.load.image('uiChildHPBar', 'img/ui/ui_child_hp_bar.png');
        game.load.image('ui_npcGuide', 'img/ui/ui_npc_guide.png');
        game.load.image('ui_npcCammie', 'img/ui/ui_npc_cammie.png');
        game.load.image('ui_npcJoJo', 'img/ui/ui_npc_jojo.png');
        game.load.image('ui_npcCaptainMarvel', 'img/ui/ui_npc_captain_marvel.png');
        game.load.image('ui_npcMario', 'img/ui/ui_npc_mario.png');
        game.load.image('ui_npcInkling', 'img/ui/ui_npc_inkling.png');
        game.load.image('ui_npcOak', 'img/ui/ui_npc_oak.png');
        game.load.image('ui_npcSonic', 'img/ui/ui_npc_sonic.png');
        game.load.image('ui_npcDinono', 'img/ui/ui_npc_dinono.png');
        game.load.image('ui_npcDinonoAndChild', 'img/ui/ui_npc_dinono_and_child.png');
        game.load.image('ui_GreenSnail', 'img/ui/ui_green_snail.png');
        game.load.image('ui_BlueSnail', 'img/ui/ui_blue_snail.png');
        game.load.image('ui_RedSnail', 'img/ui/ui_red_snail.png');
        game.load.image('ui_OrangeMushroom', 'img/ui/ui_orange_mushroom.png');
        game.load.image('ui_Skills', 'img/ui/ui_skills.png');
        game.load.image('ui_KillEnemies', 'img/ui/ui_kill_enemies.png');
        game.load.image('ui_RareCandy', 'img/ui/ui_rare_candy.png');
        game.load.image('ui_SonicRing', 'img/ui/ui_sonic_ring.png');
        game.load.image('ui_RedPotion', 'img/items/red_potion.png');
        game.load.image('ui_OrangePotion', 'img/items/orange_potion.png');
        game.load.image('ui_BluePotion', 'img/items/blue_potion.png');
        // EnemyManager
        game.load.json('enemySpawnData', 'data/enemy_spawn_data.json');
        game.load.json('enemyData', 'data/enemy_data.json');
        // Enemies
        game.load.atlasJSONHash('greenSnail', 'img/enemies/green_snail.png', 'img/enemies/green_snail.json');
        game.load.atlasJSONHash('blueSnail', 'img/enemies/blue_snail.png', 'img/enemies/blue_snail.json');
        game.load.atlasJSONHash('redSnail', 'img/enemies/red_snail.png', 'img/enemies/red_snail.json');
        game.load.atlasJSONHash('orangeMushroom', 'img/enemies/orange_mushroom.png', 'img/enemies/orange_mushroom.json');
        // NPCs
        game.load.json('npcSpawnData', 'data/npc_spawn_data.json');
        game.load.image('npcGuide', 'img/npcs/npc_guide.png');
        game.load.image('npcDinono', 'img/npcs/npc_dinono.png');
        game.load.image('npcDinonoAndChild', 'img/npcs/npc_dinono_and_child.png');
        game.load.image('npcDiyesyes', 'img/npcs/npc_diyesyes.png');
        game.load.image('npcSonic', 'img/npcs/npc_sonic.png');
        game.load.image('npcInkling', 'img/npcs/npc_inkling.png');
        game.load.image('npcCammie', 'img/npcs/npc_cammie.png');
        game.load.image('npcCaptainMarvel', 'img/npcs/npc_captain_marvel.png');
        game.load.image('npcJoJo', 'img/npcs/npc_jojo.png');
        game.load.image('npcMario', 'img/npcs/npc_mario.png');
        game.load.image('npcOak', 'img/npcs/npc_oak.png');
        // Items
        game.load.image('healParticle', 'img/items/heal_particle.png');
        game.load.image('mpParticle', 'img/items/mp_particle.png');
        game.load.image('redPotion', 'img/items/red_potion.png');
        game.load.image('orangePotion', 'img/items/orange_potion.png');
        game.load.image('bluePotion', 'img/items/blue_potion.png');
        game.load.spritesheet('sonicRing', 'img/items/sonic_ring.png', 64, 64);
        game.load.image('rareCandy', 'img/items/rare_candy.png');
        //dialogue
        game.load.image('dialogue', 'img/ui/ui_dialogue.png');
        game.load.json('dialogueData', 'data/dialogue.json');
        // TODO: ↑ ↑ ↑ LOAD ALL ASSETS HERE ↑ ↑ ↑ //

        game.load.start();
    },

    fileComplete: function (progress) {
        this.progressText.setText("Loading... " + progress + "%");
    },

    loadComplete: function () {
        this.progressText.setText("Loading FINISHED!");

        game.camera.fade('#000000', this.fadeOutTime);
        game.camera.onFadeComplete.add(this.play, this);
    },

    play: function () {
        game.state.start('play');
    }
};
