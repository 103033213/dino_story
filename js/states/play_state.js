const STATES = {
    RUNNING: Symbol("running"),
    DIALOGUE: Symbol("dialogue"),
    GAME_OVER: Symbol("game over"),
    GAME_OVER_PROCESSED: Symbol("game_over_processed")
};


// MAIN GAMEPLAY STATE
let playState = {
    stateTag: STATES.RUNNING,
    gameOver: null,
    gameOverProcessed: null, // ==true when the if statement is triggered (to prevent continuous triggering)
    bgManager: null, // class BgManager, from top_managers/bg_manager.js
    groundManager: null, // class Ground, from top_managers/ground_manager.js
    enemyManager: null, // class EnemyManager, from top_managers/obstacle_manager.js
    npcManager: null, // class NPCManager, from top_managers/npc_manager.js
    itemManager: null, // class ItemManager, from top_managers/item_manager.js
    controls: null, // class Controls, from player/controls.js
    talkControls: null, // class TalkControls, from npcs/talk_controls.js
    player: null, // class Player, from player/player.js
    dinosChild: null, // class DinosChild, from npcs/dinos_child.js
    uiManager: null, // class UiManager, from top_managers/ui_manager.js

    create: function () {

        this.stateTag = STATES.RUNNING;

        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.bgManager = new BgManager();
        this.groundManager = new GroundManager();
        this.npcManager = new NPCManager();
        this.enemyManager = new EnemyManager();
        this.controls = new Controls();
        this.talkControls = new TalkControls();
        this.player = new Player();
        this.dinosChild = new DinosChild();
        this.itemManager = new ItemManager();
        this.uiManager = new UiManager();

    },

    update: function () {
        // Check for game over -> show game over screen
        if (this.stateTag === STATES.GAME_OVER) {
            this.stateTag = STATES.GAME_OVER_PROCESSED;
            game.time.events.add(2000, function () {
                game.add.sprite(GAME_WIDTH / 2, GAME_HEIGHT / 2 - 5, 'gameOverGradient')
                    .anchor.setTo(0.5, 0.5);
                game.add.bitmapText(GAME_WIDTH / 2, GAME_HEIGHT / 2 - 20, 'silkscreen', 'GAME OVER', 48)
                    .anchor.setTo(0.5, 0.5);
                game.add.bitmapText(GAME_WIDTH / 2, GAME_HEIGHT / 2 + 20, 'silkscreen', 'Press SPACE to play again.', 24)
                    .anchor.setTo(0.5, 0.5);
            });
        }
        if (this.stateTag === STATES.GAME_OVER_PROCESSED) {
            if (this.controls.jump) {
                // Player presses space
                console.log("RESTART PLAY STATE.");
                game.state.start('play'); // Restart play state
            }
        }

        this.bgManager.update();
        this.groundManager.update();
        this.npcManager.update();
        this.enemyManager.update();
        this.controls.update();
        this.talkControls.update();
        this.player.update();
        this.dinosChild.update();
        this.itemManager.update();
        this.uiManager.update();

    },
    render: function () {
        // this.enemyManager.greenSnail.enemies.forEachAlive(function (enemy) {
        //     game.debug.body(enemy);
        // }, this);
        // this.enemyManager.blueSnail.enemies.forEachAlive(function (enemy) {
        //     game.debug.body(enemy);
        // }, this);
        // this.enemyManager.redSnail.enemies.forEachAlive(function (enemy) {
        //     game.debug.body(enemy);
        // }, this);
        // this.enemyManager.orangeMushroom.enemies.forEachAlive(function (enemy) {
        //     game.debug.body(enemy);
        // }, this);
        // game.debug.body(this.player.sprite);
        // game.debug.body(this.dinosChild.sprite);
        // game.debug.text("DEBUG COMMANDS:", 16, 150);
        // game.debug.text("Hold Z to attack.", 16, 170);
        // game.debug.text("Hold X to use skill.", 16, 190);
        // game.debug.text("Hold C to increase EXP.", 16, 210);

        // game.debug.text("CONTROL INPUTS:", 16, 240);
        // game.debug.text("Left: " + this.controls.moveLeft, 16, 260);
        // game.debug.text("Right: " + this.controls.moveRight, 16, 280);
        // game.debug.text("Jump: " + this.controls.jump, 16, 300);
        // game.debug.text("Z: " + this.controls.attack, 16, 320);
        // game.debug.text("X: " + this.controls.special1, 16, 340);
        // game.debug.text("C: " + this.controls.special2, 16, 360);
    },

    notifyEnemyKilled(enemy){
        playState.itemManager.redPotion.chanceToSpawn(40, enemy.x);
        if(enemy.enemyClass.type === "Green Snail"){
            playState.itemManager.rareCandy.chanceToSpawn(80, enemy.x);
            playState.itemManager.sonicRing.spawnRingBunch(game.rnd.between(1, 5), enemy.x);
        }
        else if (enemy.enemyClass.type === "Blue Snail"){
            playState.itemManager.bluePotion.chanceToSpawn(5, enemy.x);
            playState.itemManager.rareCandy.chanceToSpawn(15, enemy.x);
            playState.itemManager.sonicRing.spawnRingBunch(game.rnd.between(1, 10), enemy.x);
        }
        else if (enemy.enemyClass.type === "Red Snail"){
            this.itemManager.bluePotion.chanceToSpawn(10, enemy.x);
            this.itemManager.rareCandy.chanceToSpawn(100, enemy.x);
            this.itemManager.sonicRing.spawnRingBunch(game.rnd.between(1, 15), enemy.x);
        }
        else if (enemy.enemyClass.type === "Orange Mushroom"){
            playState.itemManager.bluePotion.chanceToSpawn(10, enemy.x);
            playState.itemManager.orangePotion.chanceToSpawn(10, enemy.x);
            playState.itemManager.rareCandy.chanceToSpawn(100, enemy.x);
            playState.itemManager.sonicRing.spawnRingBunch(game.rnd.between(1, 20), enemy.x);

        }

        // Let NPCManager know when an enemy dies for quest-tracking
        this.npcManager.notifyQuestNPC("Killed " + enemy.enemyClass.type);

        // Give player EXP
        this.player.currEXP += enemy.enemyClass.EXPDrop;

        // Give player job BUFF
        if(this.player.currJob === 'WARRIOR'){
            console.log("WARRIOR HEAL");
            // this.emitter = game.add.emitter(0, 0, 100);
            // this.emitter.makeParticles('healParticle');
            // this.emitter.gravity = -200;
            // this.emitter.setAlpha(1, 0.1, 1000);
            // this.emitter.x = this.player.sprite.position.x+25;
            // this.emitter.y = this.player.sprite.position.y+25;
            // this.emitter.start(true, 1000, 0, 50);
            this.player.heal(10);
        }
        if(this.player.currJob === 'DRUID'){
            console.log("DRUID WISE");
            // this.player.playerLvlManager.levelUp();
            this.lvlUpExplosion = game.add.sprite(0, 0, 'lvlUpExplosion');
            this.lvlUpExplosion.anchor.setTo(0.5, 1); // bottom middle
            game.physics.enable(this.lvlUpExplosion, Phaser.Physics.ARCADE);
            this.lvlUpExplosion.body.velocity.x = -playState.groundManager.groundSpeed;
            this.lvlUpExplosion.exists = false;
            this.lvlUpExplosionAnim = this.lvlUpExplosion.animations.add('explode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 12, false);
            this.lvlUpExplosion.reset(this.player.sprite.x + 20, this.player.sprite.y + this.player.sprite.height);
            this.lvlUpExplosion.animations.play('explode');

            this.lvlUpExplosionAnim.onComplete.add(function () {
                this.lvlUpExplosion.exists = false;
            }, this);
            this.player.currEXP += 10;
        }
    }
};
