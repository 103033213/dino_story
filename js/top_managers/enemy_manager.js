// Controls all enemies, spawns all enemies, detects enemy collisions
class EnemyManager {
    constructor() {
        this.enemySpawnData = game.cache.getJSON('enemySpawnData');

        this.greenSnail = new GreenSnail();
        this.blueSnail = new BlueSnail();
        this.redSnail = new RedSnail();
        this.orangeMushroom = new OrangeMushroom();

        this.allEnemies = [
            this.greenSnail,
            this.blueSnail,
            this.redSnail,
            this.orangeMushroom
        ];

        this.enemySpawnTimers = [];
    }

    updateLevel(currLvl) {
        // remove timers for spawning enemies of previous level
        this.enemySpawnTimers.forEach(function (timer) {
            game.time.events.remove(timer);
        });
        this.enemySpawnTimers = [];
        // get spawn data for current player level
        let currSpawnData = this.enemySpawnData[currLvl];
        // set event timers for spawning enemies
        Object.entries(currSpawnData).forEach(function (entry) {
            let enemyType = entry[0];
            let enemySpawnInterval = entry[1];
            this.enemySpawnTimers.push( game.time.events.loop(Phaser.Timer.SECOND * enemySpawnInterval, this.spawnEnemyType, this, enemyType) );
            
            console.log("1 " + enemyType + " per " + enemySpawnInterval + " second(s)");
        }, this);
    }

    spawnEnemyType(enemyType) {
        switch (enemyType) {
            case 'Green Snail':
                this.greenSnail.spawn();
                break;
            case 'Blue Snail':
                this.blueSnail.spawn();
                break;
            case 'Red Snail':
                this.redSnail.spawn();
                break;
            case 'Orange Mushroom':
                this.orangeMushroom.spawn();
                break;
            // TODO: ^^^ ADD NEW ENEMIES HERE ^^^
            default:
                console.log("No enemy type of " + enemyType + " found.");
        }
    }

    update() {
        this.greenSnail.update();
        this.blueSnail.update();
        this.redSnail.update();
        this.orangeMushroom.update();
    }
}