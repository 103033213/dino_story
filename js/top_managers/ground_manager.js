// THE GROUND THAT ALL CHARACTERS STAND ON
// There are 2 layers:
// The bottom layer (physicalGround) is the actual ground that collides with characters, but cannot see — it doesn't move.
// The top layer (visualGround) is the ground that we see — there are 2 sprites which move to the left and are looped over and over.

class GroundManager {
    constructor() {
        this.groundWidth = 1024;
        this.groundSpeed = 300;

        this.physicalGround = game.add.sprite(0, GAME_HEIGHT, 'physicalGround');
        game.physics.enable(this.physicalGround, Phaser.Physics.ARCADE);
        this.physicalGround.anchor.setTo(0, 1); // bottom left corner
        this.physicalGround.body.allowGravity = false;
        this.physicalGround.body.immovable = true;

        this.visualGround = game.add.physicsGroup();
        this.visualGround.create(0, 515, 'ground');
        this.visualGround.create(this.groundWidth, 515, 'ground');
        this.visualGround.setAll('body.velocity.x', -this.groundSpeed);
    }

    update() {
        // Loops the two visual grounds over and over to create the endless running effect
        let ground1 = this.visualGround.getChildAt(0);
        let ground2 = this.visualGround.getChildAt(1);
        if (ground1.x <= -this.groundWidth) {
            ground1.x = ground2.x + this.groundWidth;
        }
        if (ground2.x <= -this.groundWidth) {
            ground2.x = ground1.x + this.groundWidth;
        }
    }
}