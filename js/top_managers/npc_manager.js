// Controls when NPCs should spawn, and also manages quests in general
class NPCManager {
    constructor() {
        this.npcSpawnData = game.cache.getJSON('npcSpawnData');

        this.nextNPCTime = 1 * Phaser.Timer.SECOND + game.time.now;

        this.npcGuide = new NPCGuide(this);
        this.npcInkling = new NPCInkling(this);
        this.npcCammie = new NPCCammie(this);
        this.npcCaptainMarvel = new NPCCaptainMarvel(this);
        this.npcOak = new NPCOak(this);
        this.npcJoJo = new NPCJoJo(this);
        this.npcMario = new NPCMario(this);
        this.npcSonic1 = new NPCSonic1(this);
        this.npcSonic2 = new NPCSonic2(this);
        this.npcSonic3 = new NPCSonic3(this);
        this.npcDinono1 = new NPCDinono1(this);
        this.npcDinonoAndChild = new NPCDinonoAndChild(this);
        this.npcDinono2 = new NPCDinono2(this);
        this.npcDiyesyes = new NPCDiyesyes(this);
        // TODO: ^^^ ADD NEW NPC ^^^

        this.lastSpawnedNPC = null;

        // Unlocked (and not yet encountered) Quest NPCs will be in the pool to potentially show up
        this.unlockedNPCs = [];

        // Accepted (and incomplete) Quest NPCs will be in the pool to potentially show up more
        this.acceptedQuestNPCs = [];

        // Completed (but not handed in yet) Quest NPCs will be first to show up, before picking from the unlockedNPCs pool
        this.completedQuestNPCs = [];

        // Dialogue choices will be recorded here, if they need to be used
        this.dialogueChoices = {
            noChoices: 0, // this is used for NPCs that are not affected by choices, it will always be 0
            npcGuide: 0, // 1 = first time, 2 = not first time
            npcInkling: 0, // 1 = inkling is happy, 2 = inkling is angry
            npcCammie: 0, // 1 = defeat Cammie, 2 = eat Cammie
            npcOak: 0, // 1 = 法師職業, 2 = 戰士職業
            npcSonic1: 0, // 1 = jump upgrade, 2 = speed upgrade
            npcSonic2: 0, // 1 = jump upgrade, 2 = speed upgrade
            npcSonic3: 0, // 1 = jump upgrade, 2 = speed upgrade
            npcDinono1: 0, // 1 = maybe, 2 = wtf
            npcDiyesyes: 0, // 1 = sell child, 2 = don't sell, 3 = child died before meeting Diyesyes
            
            npcCaptainMarvel :0, //notthing
            npcJoJo :0, //notthing
            npcDinonoAndChild :0, //notthing
            npcMario :0, //notthing
            npcDinono2 :0 //notthing
            // TODO: ^^^ ADD NEW DIALOGUE CHOICES ^^^
        };
    }

    // When the player levels up, unlock new NPCs for that level
    updateLevel(currLvl) {
        let currSpawnData = this.npcSpawnData[currLvl];
        currSpawnData.forEach(function (npc) {
            this.unlockNPC(npc);
        }, this);
    }

    // Add an NPC to the unlocked pool
    unlockNPC(npc) {
        let npcToUnlock = null;
        switch (npc) {
            case "npcGuide":
                npcToUnlock = this.npcGuide;
                break;
            case "npcInkling":
                npcToUnlock = this.npcInkling;
                break;
            case "npcCammie":
                npcToUnlock = this.npcCammie;
                break;
            case "npcCaptainMarvel":
                npcToUnlock = this.npcCaptainMarvel;
                break;
            case "npcOak":
                npcToUnlock = this.npcOak;
                break;
            case "npcJoJo":
                npcToUnlock = this.npcJoJo;
                break;
            case "npcMario":
                npcToUnlock = this.npcMario;
                break;
            case "npcSonic1":
                npcToUnlock = this.npcSonic1;
                break;
            case "npcSonic2":
                npcToUnlock = this.npcSonic2;
                break;
            case "npcSonic3":
                npcToUnlock = this.npcSonic3;
                break;
            case "npcDinono1":
                npcToUnlock = this.npcDinono1;
                break;
            case "npcDinonoAndChild":
                npcToUnlock = this.npcDinonoAndChild;
                break;
            case "npcDinono2":
                npcToUnlock = this.npcDinono2;
                break;
            case "npcDiyesyes":
                npcToUnlock = this.npcDiyesyes;
                break;
            // TODO: ^^^ ADD NEW NPC ^^^
        }
        if (npcToUnlock !== null) {
            this.unlockedNPCs.push(npcToUnlock);
        }
    }

    // Unlock items that are only available during quests
    unlockItemSpawn(npc) {
        switch (npc) {
            case this.npcSonic1:
                playState.itemManager.sonicRing.spawnLock = false;
                console.log("Unlocked item spawn: Sonic Ring");
                break;
            case this.npcSonic2:
                playState.itemManager.sonicRing.spawnLock = false;
                console.log("Unlocked item spawn: Sonic Ring");
                break;
            case this.npcSonic3:
                playState.itemManager.sonicRing.spawnLock = false;
                console.log("Unlocked item spawn: Sonic Ring");
                break;
            case this.npcOak:
                playState.itemManager.rareCandy.spawnLock = false;
                console.log("Unlocked item spawn: Rare Candy");
                break;
        }
    }

    // Lock items that are only available during quests
    lockItemSpawn(npc) {
        switch (npc) {
            case this.npcSonic1:
                playState.itemManager.sonicRing.spawnLock = true;
                console.log("Locked item spawn: Sonic Ring");
                break;
            case this.npcSonic2:
                playState.itemManager.sonicRing.spawnLock = true;
                console.log("Locked item spawn: Sonic Ring");
                break;
            case this.npcSonic3:
                playState.itemManager.sonicRing.spawnLock = true;
                console.log("Locked item spawn: Sonic Ring");
                break;
            case this.npcOak:
                playState.itemManager.rareCandy.spawnLock = true;
                console.log("Locked item spawn: Rare Candy");
                break;
        }
    }

    // Keeps track of quest related events that happen in other parts of code.
    // Distributes this information to all currently quest-tracking NPCs.
    notifyQuestNPC(questEvent) {
        this.acceptedQuestNPCs.forEach(function (npc) {
            npc.trackQuest(questEvent);
        });
    }

    update() {
        // Try to spawn an NPC at set intervals
        if (game.time.now > this.nextNPCTime) {
            // console.log("Unlocked Quests: "+this.unlockedNPCs.length);
            // console.log("Accepted Quests: "+this.acceptedQuestNPCs.length);
            // console.log("Completed Quests: "+this.completedQuestNPCs.length);

            // Try to spawn an NPC with a completed quest first
            if (this.completedQuestNPCs.length > 0) {
                this.lastSpawnedNPC = this.completedQuestNPCs[0];
                this.lastSpawnedNPC.spawn();
            }
            // If no quests are completed, try to spawn any unlocked quest
            else if (this.unlockedNPCs.length > 0) {
                this.lastSpawnedNPC = Phaser.ArrayUtils.getRandomItem(this.unlockedNPCs, 0, this.unlockedNPCs.length);
                this.lastSpawnedNPC.spawn();
            }
            this.nextNPCTime = game.time.now + game.rnd.between(5, 7) * Phaser.Timer.SECOND;
        }

        if (this.lastSpawnedNPC) {
            this.lastSpawnedNPC.update(); // This only works because there should only be one NPC on the screen at a time
        }
    }
}
