// SHOWS ALL UI ELEMENTS, SUCH AS LEVEL, HP, MP, ETC

class UiManager {
    constructor() {
        // Player Info
        this.playerInfo = game.add.sprite(20, 20, 'uiPlayerInfo');
        this.playerInfo.scale.setTo(0.5, 0.5);
        this.HPBar = game.add.sprite(119, 41, 'uiHPBar');
        this.HPBar.scale.setTo(0.5, 0.5);
        this.MPBar = game.add.sprite(119, 63, 'uiMPBar');
        this.MPBar.scale.setTo(0.5, 0.5);
        this.HPText = game.add.bitmapText(120, 41, 'silkscreen', "HP: 100/100", 18);
        this.MPText = game.add.bitmapText(120, 63, 'silkscreen', "MP: 50/50", 12);
        this.jobText = game.add.bitmapText(115, 89, 'silkscreen', "Lvl 1 NEWBIE", 16);

        // EXP Info
        this.EXPInfo = game.add.sprite(GAME_WIDTH / 2, GAME_HEIGHT - 15, 'uiEXPInfo');
        this.EXPInfo.scale.setTo(0.5, 0.5);
        this.EXPInfo.anchor.setTo(0.5, 1); // bottom middle
        this.EXPBar = game.add.sprite(GAME_WIDTH / 2 - 250, GAME_HEIGHT - 30, 'uiEXPBar');
        this.EXPBar.scale.setTo(0.5, 0.5);
        this.EXPText = game.add.bitmapText(GAME_WIDTH / 2, GAME_HEIGHT - 21, 'silkscreen', "EXP: 100", 15);
        this.EXPText.anchor.setTo(0.5, 0.5);
        this.nextLvlText = game.add.bitmapText(GAME_WIDTH / 2, GAME_HEIGHT - 44, 'silkscreen', "EXP TO NEXT LEVEL: 100", 19);
        this.nextLvlText.anchor.setTo(0.5, 0.5);

        // Quests Info
        this.questsTopText = game.add.bitmapText(GAME_WIDTH - 130, 30, 'silkscreen', "Active Quests", 18);
        this.questsTopText.anchor.setTo(0.5, 0.5);
        this.questsBg = game.add.sprite(GAME_WIDTH - 130, 50, 'uiQuestsBg');
        this.questsBg.anchor.setTo(0.5, 0);
        this.questsBg.scale.setTo(0.5, 1);
        this.questsBg.height = 30;
        this.questsDefaultText = game.add.bitmapText(GAME_WIDTH - 130, 60, 'silkscreen', "(No Active Quests)", 14);
        this.questsDefaultText.anchor.setTo(0.5, 0);
        this.activeQuests = [];
        this.activeQuestUIs = [];
        this.questUIDistance = 60;

        // Dino's Child Info
        this.childInfo = game.add.sprite(25, 125, 'uiChildInfo');
        this.childInfo.scale.setTo(0.25, 0.25);
        this.childHPBar = game.add.sprite(75, 139, 'uiChildHPBar');
        this.childHPBar.scale.setTo(0.25, 0.25);
        this.childHPText = game.add.bitmapText(80, 142, 'silkscreen', "HP: 100/100", 14);
        this.childInfo.visible = false;
        this.childHPBar.visible = false;
        this.childHPText.visible = false;
    }

    update() {
        if (playState.player.stats.currHP <= 0) {
            this.HPText.text = "HP: 0/" + playState.player.stats.maxHP;
            this.HPBar.scale.x = 0;
        } else {
            this.HPText.text = "HP: " + playState.player.stats.currHP + "/" + playState.player.stats.maxHP;
            this.HPBar.scale.x = playState.player.stats.currHP / playState.player.stats.maxHP * 0.5;
        }
        if (playState.player.stats.currMP <= 0) {
            this.MPText.text = "MP: 0/" + playState.player.stats.maxMP;
            this.MPBar.scale.x = 0;
        } else {
            this.MPText.text = "MP: " + playState.player.stats.currMP + "/" + playState.player.stats.maxMP;
            this.MPBar.scale.x = playState.player.stats.currMP / playState.player.stats.maxMP * 0.5;
        }
        if (playState.player.nextLvlEXP === "MAX") {
            this.EXPBar.scale.x = 0.5;
        } else {
            this.EXPBar.scale.x = (playState.player.currEXP - playState.player.prevLvlEXP) / (playState.player.nextLvlEXP - playState.player.prevLvlEXP) * 0.5;
        }

        this.jobText.text = "Lvl " + playState.player.currLvl + " " + playState.player.currJob;
        this.EXPText.text = "EXP: " + playState.player.currEXP;
        this.nextLvlText.text = "EXP TO NEXT LEVEL: " + playState.player.nextLvlEXP;

        if (playState.dinosChild.dead) {
            this.childHPText.text = "DEAD";
            this.childHPBar.visible = false;
        } else if (playState.dinosChild.sold) {
            this.childHPText.text = "SOLD";
            this.childHPBar.visible = false;
        } else {
            this.childHPText.text = "HP: " + playState.dinosChild.stats.currHP + "/" + playState.dinosChild.stats.maxHP;
            this.childHPBar.scale.x = playState.dinosChild.stats.currHP / playState.dinosChild.stats.maxHP * 0.25;
        }
    }

    addActiveQuest(quest) {
        let activeQuestNum = this.activeQuests.length;
        this.questsBg.height = 30 + (activeQuestNum + 1) * this.questUIDistance;
        this.questsDefaultText.visible = false;

        let npcImg = game.add.sprite(GAME_WIDTH - 200, 95 + this.questUIDistance * activeQuestNum, 'ui_' + quest.loadSprite);
        npcImg.anchor.setTo(0.5, 0.5);
        npcImg.scale.setTo(0.5, 0.5);
        let progress = quest.questProgress + '/' + quest.questGoal;
        let progressText = game.add.bitmapText(GAME_WIDTH - 130, 95 + this.questUIDistance * activeQuestNum, 'silkscreen', progress, 18);
        progressText.anchor.setTo(0.5, 0.5);
        let targetImg = game.add.sprite(GAME_WIDTH - 60, 95 + this.questUIDistance * activeQuestNum, 'ui_' + quest.questTarget);
        targetImg.anchor.setTo(0.5, 0.5);
        targetImg.scale.setTo(0.75, 0.75);

        this.activeQuests.push(quest);
        this.activeQuestUIs.push({npcImg: npcImg, progressText: progressText, targetImg: targetImg});
    }

    removeActiveQuest(quest) {
        let activeQuestNum = this.activeQuests.length;
        this.questsBg.height = 30 + (activeQuestNum - 1) * this.questUIDistance;
        this.questsDefaultText.visible = (activeQuestNum - 1) === 0;

        // Remove quest and its ui from active quest lists
        let index = this.activeQuests.indexOf(quest);
        if (index > -1) {
            this.activeQuests.splice(index, 1);
            // Remove UI elements
            game.world.remove(this.activeQuestUIs[index].npcImg);
            game.world.remove(this.activeQuestUIs[index].progressText);
            game.world.remove(this.activeQuestUIs[index].targetImg);
            this.activeQuestUIs.splice(index, 1);
        }

        for (let i = index; i < activeQuestNum - 1; i++) {
            let questUI = this.activeQuestUIs[i];
            questUI.npcImg.y = 95 + this.questUIDistance * i;
            questUI.progressText.y = 95 + this.questUIDistance * i;
            questUI.targetImg.y = 95 + this.questUIDistance * i;
        }
    }

    trackActiveQuests() {
        let activeQuestNum = this.activeQuests.length;

        for (let i = 0; i < activeQuestNum; i++) {
            let questNPC = this.activeQuests[i];
            let questUI = this.activeQuestUIs[i];
            let text;
            if (questNPC.questProgress >= questNPC.questGoal) {
                text = 'DONE!'
            } else {
                text = questNPC.questProgress + '/' + questNPC.questGoal;
            }
            questUI.progressText.setText(text);
        }
    }
}